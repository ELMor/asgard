%% @author linarese
%% @doc @todo Add description to kitchen.


-module(kitchen).

%% ====================================================================
%% API functions
%% ====================================================================
-compile(export_all).

fridge1() ->
	receive
		{From, {store, _Food}} ->
			From ! {self(), ok},
			fridge1();
		{From, {take, _Food}} ->
			%% uh....
			From ! {self(), not_found},
			fridge1();
		terminate -> ok 
	end.

%% ====================================================================
%% Internal functions
%% ====================================================================


