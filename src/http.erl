%% @author linarese
%% @doc @todo Add description to http.


-module(http).
-export([
  run_server/0
]).

run_server() ->
  ok = inets:start(),
  {_, Result} = inets:start(httpd, [
    							{port, 8080},
							    {server_name, "hello_erlang"},
							    {server_root, "/Users/linarese/workspace/Asgard"},
							    {document_root, "/Users/linarese/workspace/Asgard"},
							    {bind_address, "localhost"}
							  ]
					   ),
	Result.
   